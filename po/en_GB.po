# British (English) translation for pessulus
# Copyright (C) 2006 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the pessulus package.
# David Lodge <dave@cirt.net>, 2006
# Philip Withnall <philip@tecnocode.co.uk>, 2009.
# Bruce Cowan <bruce@bcowan.me.uk>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: pessulus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-06-19 00:36+0100\n"
"PO-Revision-Date: 2010-06-19 00:38+0100\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: British English <en@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.6.1\n"

#: ../data/pessulus.desktop.in.in.h:1
msgid "Configure the lockdown policy"
msgstr "Configure the lockdown policy"

#: ../data/pessulus.desktop.in.in.h:2 ../data/pessulus.ui.h:3
#: ../Pessulus/main.py:88
msgid "Lockdown Editor"
msgstr "Lockdown Editor"

#: ../data/pessulus.ui.h:1
msgid "Disable _unsafe protocols"
msgstr "Disable _unsafe protocols"

#: ../data/pessulus.ui.h:2
msgid "Disabled Applets"
msgstr "Disabled Applets"

#: ../data/pessulus.ui.h:4
msgid "Safe Protocols"
msgstr "Safe Protocols"

#: ../Pessulus/lockdownbutton.py:100
msgid "Click to make this setting not mandatory"
msgstr "Click to make this setting not mandatory"

#: ../Pessulus/lockdownbutton.py:102
msgid "Click to make this setting mandatory"
msgstr "Click to make this setting mandatory"

#: ../Pessulus/maindialog.py:40
msgid "General"
msgstr "General"

#: ../Pessulus/maindialog.py:41
msgid "Panel"
msgstr "Panel"

#: ../Pessulus/maindialog.py:42
msgid "Epiphany Web Browser"
msgstr "Epiphany Web Browser"

#: ../Pessulus/maindialog.py:43
msgid "GNOME Screensaver"
msgstr "GNOME Screensaver"

#: ../Pessulus/maindialog.py:53
msgid "Disable _command line"
msgstr "Disable _command line"

#: ../Pessulus/maindialog.py:54
msgid "Disable _printing"
msgstr "Disable _printing"

#: ../Pessulus/maindialog.py:55
msgid "Disable print _setup"
msgstr "Disable print _setup"

#: ../Pessulus/maindialog.py:56
msgid "Disable save to _disk"
msgstr "Disable save to _disk"

#: ../Pessulus/maindialog.py:58
msgid "_Lock down the panels"
msgstr "_Lock down the panels"

#: ../Pessulus/maindialog.py:59
msgid "Disable force _quit"
msgstr "Disable force _quit"

#: ../Pessulus/maindialog.py:60
msgid "Disable log _out"
msgstr "Disable log _out"

#: ../Pessulus/maindialog.py:62
msgid "Disable _quit"
msgstr "Disable _quit"

#: ../Pessulus/maindialog.py:63
msgid "Disable _arbitrary URL"
msgstr "Disable _arbitrary URL"

#: ../Pessulus/maindialog.py:64
msgid "Disable _bookmark editing"
msgstr "Disable _bookmark editing"

#: ../Pessulus/maindialog.py:65
msgid "Disable _history"
msgstr "Disable _history"

#: ../Pessulus/maindialog.py:66
msgid "Disable _javascript chrome"
msgstr "Disable _javascript chrome"

#: ../Pessulus/maindialog.py:67
msgid "Disable _toolbar editing"
msgstr "Disable _toolbar editing"

#: ../Pessulus/maindialog.py:68
msgid "Force _fullscreen mode"
msgstr "Force _fullscreen mode"

#: ../Pessulus/maindialog.py:69
msgid "Hide _menubar"
msgstr "Hide _menubar"

#: ../Pessulus/maindialog.py:71
msgid "Disable lock _screen"
msgstr "Disable lock _screen"

#: ../Pessulus/maindialog.py:72
msgid "_Lock on activation"
msgstr "_Lock on activation"

#: ../Pessulus/maindialog.py:73
msgid "Allow log _out"
msgstr "Allow log _out"

#: ../Pessulus/maindialog.py:74
msgid "Allow user _switching"
msgstr "Allow user _switching"

#: ../Pessulus/maindialog.py:233
#, python-format
msgid "Could not display help document '%s'"
msgstr "Could not display help document '%s'"

#: ../Pessulus/main.py:38
msgid "Could not import the bugbuddy Python bindings"
msgstr "Could not import the bugbuddy Python bindings"

#: ../Pessulus/main.py:39
msgid "Make sure you have the bugbuddy Python bindings installed"
msgstr "Make sure you have the bugbuddy Python bindings installed"

#: ../Pessulus/main.py:83
msgid "Cannot contact the GConf server"
msgstr "Cannot contact the GConf server"

#: ../Pessulus/main.py:84
msgid ""
"This usually happens when running this application with 'su' instead of 'su "
"-'.\n"
"If this is not the case, you can look at the output of the application to "
"get more details."
msgstr ""
"This usually happens when running this application with 'su' instead of 'su "
"-'.\n"
"If this is not the case, you can look at the output of the application to "
"get more details."
